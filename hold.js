// Add authorization before allowing access to the data or users
function auth(req, res, next) {
    console.log(req.headers);
  
    var authHeader = req.headers.authorization;
  
    if (!authHeader) {
      // challenge if they didn't provide authorization info in header
      var err = new Error('You are not authenticated!');
  
      res.setHeader('WWW-Authenticate', 'Basic');
      err.status = 401;
      return next(err);
    }
  
    var auth = new Buffer.from(authHeader.split(' ')[1], 'base64').toString().split(':');
    
    var username = auth[0];
    var password = auth[1];
  
    if ( username === 'admin' && password === 'password' ) {
      next();
    } else {
      var err = new Error('Username & password not authenticated!');
  
      res.setHeader('WWW-Authenticate', 'Basic');
      err.status = 401;
      return next(err);   
    }
  }
  
  app.use(auth);
