const express = require(`express`);
const bodyParser = require(`body-parser`);
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const cors = require('./cors');

const Favorites = require('../models/favorite');

const favoriteRouter = express.Router();
favoriteRouter.use(bodyParser.json());

const dataType = `favorite`;
const dataTypePlural = `favorites`;

favoriteRouter.route(`/`)
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors, authenticate.verifyUser, (req,res,next) => {
    // console.log("here is the req object: %o", req);
    Favorites.findOne({user: req.user._id})
    .populate('user')
    .populate({
        path:'dishes',
        model: 'Dish'
    })
    .then((favorite) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(favorite);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req,res,next) => {

    // check to see if favorite dishes coming in is an array and if not, convert it to one
    let fav_dishes = req.body;
    if (!Array.isArray(fav_dishes)) {
        let dishes_array = [];
        dishes_array.push(fav_dishes);
        fav_dishes = dishes_array;
    }

    // Set options to insert a new doc if one is not found (upsert)
    // to return the updated document (new)
    // and to use the Schema defaults when creating a new document
    let options = { upsert: true, new: true, setDefaultsOnInsert: true };

    // Find the document (or create it) and then update it
    // the $addToSet does a push for each element of the incoming array (fav_dishes) 
    // the $each option makes sure it is unique
    Favorites.findOneAndUpdate({user: req.user._id}, { $addToSet: { dishes: { $each: fav_dishes }}}, options )
        .then((favorite) => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(favorite);
            }, (err) => next(err))
        .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser, (req,res,next) => {
    res.statusCode = 403;
    res.end(`PUT operation not supported on /${dataTypePlural}`);
})
.delete(cors.corsWithOptions, authenticate.verifyUser, (req,res,next) => {
    Favorites.remove({})
    .then((resp) => {
        console.log('All Favorites Deleted');
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

favoriteRouter.route(`/:itemId`)
.options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
.get(cors.cors, (req,res,next) => {
    // GET is not supported on just one favorite
    res.statusCode = 403;
    res.end(`GET operation not supported on /${dataTypePlural}/` + req.params.itemId + 
        ` Try just /${dataTypePlural}/`);
})
.post(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req,res,next) => {

    // change dishID (itemId) to an array to enable using the $addToSet & $each options to 
    // add only unique IDs
    let fav_dishes = JSON.parse('{"_id": "' + req.params.itemId + '"}');
    if (!Array.isArray(fav_dishes)) {
        let dishes_array = [];
        dishes_array.push(fav_dishes);
        fav_dishes = dishes_array;
    }

    // Set options to insert a new doc if one is not found (upsert)
    // to return the updated document (new)
    // and to use the Schema defaults when creating a new document
    let options = { upsert: true, new: true, setDefaultsOnInsert: true };

    // Find the document (or create it) and then update it
    // the $addToSet does a push for each element of the incoming array (fav_dishes) 
    // the $each option makes sure it is unique
    Favorites.findOneAndUpdate({user: req.user._id}, { $addToSet: { dishes: { $each: fav_dishes }}}, options )
        .then((favorite) => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(favorite);
            }, (err) => next(err))
        .catch((err) => next(err));
})
.put(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req,res,next) => {
    // PUT is not supported on just one favorite
    res.statusCode = 403;
    res.end(`PUT operation not supported on /${dataTypePlural}/` + req.params.itemId);
})
.delete(cors.corsWithOptions, authenticate.verifyUser, authenticate.verifyAdmin, (req,res,next) => {

    let options = { upsert: true, new: true, setDefaultsOnInsert: true };

    Favorites.findOneAndUpdate(
        { user: req.user._id }, 
        { $pull: { 'dishes': req.params.itemId }},
        options
    )
    .then((resp) => {
        console.log('Favorite Deleted %o', resp);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));
});

module.exports = favoriteRouter;